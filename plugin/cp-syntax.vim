" set syntax for cplane logs
autocmd BufRead,BufNewFile *.shb.txt,*.out,*.log,*.LOG set filetype=nsn-syslog
autocmd BufRead,BufNewFile *.k3.txt set filetype=nsn-k3txt
autocmd BufRead,BufNewFile *.k3.log set filetype=k3p

