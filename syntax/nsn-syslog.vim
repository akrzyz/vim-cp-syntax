
if exists("b:current_syntax")
  finish
endif

"CPLANE COMPONENTS
syn keyword syslogCpComp1 RROMexe cp_nb
syn keyword syslogCpComp2 CELLCex cp_cl
syn keyword syslogCpComp3 ENBCexe cp_if
syn keyword syslogCpComp4 UECexe  cp_ue
syn keyword syslogCpComp5 TUPcexe cp_sctp
syn keyword syslogCpComp6 BTSOMex
syn cluster syslogCpComponents contains=syslogCpComp1,syslogCpComp2,syslogCpComp3,syslogCpComp4,syslogCpComp5,syslogCpComp6

"LOG
"syn match syslogLog ".*" contained
" timer
syn match syslogTimer "\c\<timer\>"
syn match syslogExpired "\c\<expired\>"
syn match syslogExpired "\c\<timeout\>"
syn match syslogExpired "\<[A-Z_]*EXPIRED[A-Z_]*\>"
syn cluster syslogLogTimers contains=syslogTimer,syslogExpired

" error
syn match Error "ERR/CCS/AaError"
syn match syslogUknown "[Uu]nknown"
syn cluster syslogLogErrors contains=syslogUknown

syn match syslogHexNumber "0x[0-9a-fA-F]\+"

" events and msgs
syn match syslogEvent "\c\<event[a-zA-Z]*"
syn match syslogMsg   "\c\<\(message\|msg\)[a-zA-Z]*"
syn match syslogMessages "\c\<\w\+\(resp\|req\|ind\|ack\)\>"
syn cluster syslogLogEventsAndMessages contains=syslogEvent,syslogMsg,syslogMessages

syn match syslogLogEq "[:=]" skipwhite nextgroup=syslogLogNumericValue
syn match syslogLogNumericValue "\<[0-9A-Fa-f]\+\>" contained

"ENB SYSLOG FORMAT:
"$syslogGarbage $syslogLocation <$syslogDateAndTime> $syslogLogLvlAndFileName $syslogLog
syn match syslogGarbage "^[0-9 \.:\[\]]*[0-9a-f]\{2}" skipwhite nextgroup=syslogLocation
"syslog loaction
syn match syslogLocation "\S\+" contained contains=syslogNodeAndBoard skipwhite nextgroup=syslogDateAndTime
syn match syslogNodeAndBoard "[A-Z0-9]\+[_-][A-Z0-9]\+" contained nextgroup=syslogProcName
syn match syslogProcName "\S*\>" contained contains=@syslogCpComponents
" date and time
syn match syslogDateAndTime "<\S\+>" contained contains=syslogDate,syslogTime skipwhite nextgroup=syslogThreadName
syn match syslogTime "\d\d:\d\d:\d\d" contained
syn match syslogDate "\d\{4}\-\d\{2}\-\d\{2}" transparent contained
syn match syslogThreadName "\S\+" contained skipwhite nextgroup=syslogLogLvlAndFileName
"log level and file name
syn match syslogLogLvlAndFileName "\S\+" contained contains=@syslogLogLvls "skipwhite nextgroup=syslogLog
syn match syslogDbg "\<DBG/\S*\>" contained contains=syslogLineNumber
syn match syslogInf "\<INF/\S*\>" contained contains=syslogLineNumber
syn match syslogVip "\<VIP/\S*\>" contained contains=syslogLineNumber
syn match syslogErr "\<ERR/\S*\>" contained contains=syslogLineNumber
syn match syslogWrn "\<WRN/\S*\>" contained contains=syslogLineNumber
syn match syslogLineNumber "[#:=][0-9]\+" contained
syn cluster syslogLogLvls contains=syslogDbg,syslogInf,syslogVip,syslogWrn,syslogErr


"GNB-CU SYSLOG FORMAT:
"$syslogDateT$syslogTime/\d\+/$logLvl/$component/$fileName $log
syn match syslogGnbCuDateAndTime "^[0-9\-\.\:T]\+/\d\+" contains=syslogDate,syslogTime nextgroup=@syslogGnbCuLogLvlAndFileName
syn match syslogGnbCuDebug "/DBG/\S\+ "   contained contains=syslogLineNumber,@syslogCpComponents
syn match syslogGnbCuInfo  "/INF/\S\+ "   contained contains=syslogLineNumber,@syslogCpComponents
syn match syslogGnbCuWarn  "/WRN/\S\+ "   contained contains=syslogLineNumber,@syslogCpComponents
syn match syslogGnbCuError "/ERR/\S\+ "   contained contains=syslogLineNumber,@syslogCpComponents
syn match syslogGnbCuTrace "/TRACE/\S\+ " contained contains=syslogLineNumber,@syslogCpComponents
syn cluster syslogGnbCuLogLvlAndFileName contains=syslogGnbCuDebug,syslogGnbCuTrace,syslogGnbCuInfo,syslogGnbCuWarn,syslogGnbCuError

"[$time][$component][$proc][$proc][$logLvl] $filename $log
"syn match syslogGnbCuDateAndTime "^\[[0-9:\.]\+\]" nextgroup=syslogGnbCuComponent
"syn match syslogGnbCuTime "^\[[0-9:\.]\+\]"  contains=syslogTime nextgroup=syslogGnbCuComponent
"syn match syslogGnbCuComponent "\[\w\+\]"    contained contains=syslogGnbCuComponentName nextgroup=syslogGnbCuProc
"syn match syslogGnbCuComponentName "\w\+"    contained contains=@syslogCpComponents
"syn match syslogGnbCuProc "\[\w\+\]\[\w\+\]" contained nextgroup=@syslogGnbCuLogLvlAndFileName
"
"syn match syslogGnbCuDebug "\[debug] \S\+"   contained contains=syslogLineNumber
"syn match syslogGnbCuTrace "\[trace] \S\+"   contained contains=syslogLineNumber
"syn match syslogGnbCuInfo  "\[info] \S\+"    contained contains=syslogLineNumber
"syn match syslogGnbCuWarn  "\[warning] \S\+" contained contains=syslogLineNumber
"syn match syslogGnbCuError "\[error] \S\+"   contained contains=syslogLineNumber
"syn cluster syslogGnbCuLogLvlAndFileName contains=syslogGnbCuDebug,syslogGnbCuTrace,syslogGnbCuInfo,syslogGnbCuWarn,syslogGnbCuError

"GNB-DU SYSLOG FORMAT:
"$dateT$time/$logLvl/$fileName $log
syn match syslogGnbDuDateAndTime "^\d\{8}T\d\{6}\.\d\{6}\/" nextgroup=syslogLogLvlAndFileName

hi syslogCpComp1               guifg=IndianRed                      gui=BOLD      ctermbg=brown     ctermfg=white     cterm=BOLD
hi syslogCpComp2               guifg=cyan                           gui=BOLD      ctermbg=cyan      ctermfg=white     cterm=BOLD
hi syslogCpComp3               guifg=YellowGreen                    gui=BOLD      ctermbg=magenta   ctermfg=white     cterm=BOLD
hi syslogCpComp4               guifg=SteelBlue1                     gui=BOLD      ctermbg=blue      ctermfg=white     cterm=BOLD
hi syslogCpComp5               guifg=MediumOrchid                   gui=BOLD      ctermbg=green     ctermfg=white     cterm=BOLD
hi syslogCpComp6               guifg=orange                         gui=BOLD      ctermbg=yellow    ctermfg=white     cterm=BOLD
"syslog log
hi syslogTimer                 guifg=violet                         gui=BOLD      ctermfg=yellow                      cterm=BOLD
hi syslogUknown                guifg=Yellow                         gui=BOLD      ctermfg=yellow                      cterm=BOLD
hi syslogExpired               guifg=Yellow                         gui=BOLD      ctermfg=yellow                      cterm=BOLD
hi syslogHexNumber                                                  gui=BOLD                                          cterm=BOLD
hi syslogEvent                 guifg=khaki3                         gui=BOLD                                          cterm=BOLD
hi syslogMsg                   guifg=cornsilk                       gui=BOLD                                          cterm=BOLD
hi syslogMessages              guifg=khaki                          gui=BOLD                                          cterm=BOLD
hi syslogLogNumericValue       guifg=MistyRose
"syslog  enb
hi syslogGarbage               guifg=darkgray                                     ctermfg=darkgray
hi syslogNodeAndBoard          guifg=cyan                                         ctermfg=cyan
hi syslogDateAndTime           guifg=darkgray                                     ctermfg=cyan
hi syslogTime                  guifg=Khaki                                        ctermfg=yellow                      cterm=BOLD

hi syslogDbg                   guifg=LightSteelBlue                 gui=BOLD      ctermfg=darkgray                    cterm=BOLD
hi syslogInf                   guifg=MediumSeaGreen                 gui=BOLD      ctermfg=darkgreen                   cterm=BOLD
hi syslogVip                   guifg=Gold                           gui=BOLD      ctermfg=darkgreen                   cterm=BOLD
hi syslogWrn                   guifg=gold                           gui=BOLD      ctermfg=darkyellow                  cterm=BOLD
hi syslogErr                   guifg=white          guibg=red       gui=BOLD      ctermfg=darkgray                    cterm=BOLD
hi syslogLineNumber            guifg=MistyRose                                                       ctermfg=darkgray
"gnb-cu
hi syslogGnbCuTime             guifg=darkgray                                     ctermfg=darkgray
hi syslogGnbCuComponent        guifg=darkgray                                     ctermfg=darkgray
hi syslogGnbCuProc             guifg=darkgray                                     ctermfg=darkgray
hi syslogGnbCuComponentName    guifg=white                          gui=BOLD

hi syslogGnbCuDebug            guifg=LightSteelBlue                 gui=BOLD      ctermfg=darkgray                    cterm=BOLD
hi syslogGnbCuTrace            guifg=Gold                           gui=BOLD      ctermfg=darkgreen                   cterm=BOLD
hi syslogGnbCuInfo             guifg=MediumSeaGreen                 gui=BOLD      ctermfg=darkgreen                   cterm=BOLD
hi syslogGnbCuWarn             guifg=gold                           gui=BOLD      ctermfg=darkyellow                  cterm=BOLD
hi syslogGnbCuError            guifg=white          guibg=red       gui=BOLD      ctermfg=darkgray                    cterm=BOLD

"gnb-du
hi syslogGnbDuDateAndTime      guifg=darkgray                                     ctermfg=darkgray

let b:current_syntax = "nsn-syslog"
